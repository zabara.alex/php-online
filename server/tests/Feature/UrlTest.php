<?php

namespace Tests\Feature;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UrlTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testHomePage()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCategoryIndex()
    {
        $response = $this->get(route('home.category.index'));

        $response->assertStatus(200);
    }

    public function testCategoryStore()
    {
        $response = $this->post(route('home.category.store', [
            'title' => 'test.qweqwe',
            'description' => 'test.wqeeqwewqewq',
        ]));


        $response->assertStatus(302);


    }
}
