<?php

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/product', function (Product $product) {
    return $product->all()->toJson();
});

Route::get('/product/{id}', function ($id, Product $product) {
    return $product->where('id', '=', $id)->first()->toJson();
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
