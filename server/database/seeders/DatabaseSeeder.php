<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Seeder;
use App\Models\Flight;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        User::factory(10)->create();
//        Product::factory(20)->create();
//        Category::factory(5)->create();
//        Brand::factory(4)->create();
//        Flight::factory(4)->create();

//        Customer::factory(4)->create();
//        Comment::factory(10)->create();
        Order::factory(10)->create();
    }
}
