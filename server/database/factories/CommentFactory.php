<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'customer_id' => 2,
            'title' => $this->faker->realText(15),
            'description' => $this->faker->realText(150),
        ];
    }
}
