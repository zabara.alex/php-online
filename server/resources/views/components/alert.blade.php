<div>
    <span>{{ $title }}</span>

    <div>
        {{ $slot }}
    </div>

    <div>
        {{ $name }}
    </div>
</div>
