<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Customer;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Testing\Fluent\Concerns\Has;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(Customer $customer)
    {



//        $p = Product::find(1);
//
//        $array = [123, 234, 456];
//
//        $p->title = $array;
//
//        dd($p->title);





//        $p->title = '<h1>' . $p->title . '</h1>';
//        dd($p->FullProductName);
//echo $p->title;
//        $customer = $customer::find(5)->comments;
//        $comment = Comment::find(1);

//        $cat = Category::find(1);
//        dd($cat->product);

//        $p = Product::find(2);
//        dd($p->category);

//        dd($comment->user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $f = self::fAG();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $f = self::fAG();
        if ($request->file('myFile')->isValid()) {
            $request->file('myFile')->storeAs('images', 'Test.png');
        }
//        dd($request->user());
        dd($request->myFile->path());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $f = self::fAG();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
