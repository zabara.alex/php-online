<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategoryRequest;
use App\Jobs\ProcessPodcast;
use App\Models\Category;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Telegram\Bot\Laravel\Facades\Telegram;

class CategoryController extends Controller
{
    /**
     * @param Category $category
     * @return View
     */
    public function index(Category $category): View
    {
        return view('home.category.index', ['categories' => $category->all()]);
    }

    /**
     * @param StoreCategoryRequest $storeCategoryRequest
     * @param Category $category
     * @return RedirectResponse
     */
    public function store(StoreCategoryRequest $storeCategoryRequest, Category $category): RedirectResponse
    {
        try {
            $category = $category::create($storeCategoryRequest->all());
//            Mail::se
        } catch (Exception $exception) {
            Log::error($exception);
        }

        ProcessPodcast::dispatch($category);

        return redirect()->route('home.category.index');
    }

    /**
     * @param $id
     * @param Category $category
     * @return RedirectResponse
     */
    public function destroy($id, Category $category): RedirectResponse
    {
        $category::destroy($id);
        return redirect()->route('home.category.index');
    }

    /**
     * @param Category $category
     * @return View
     */
    public function show(Category $category): View
    {
        return view('home.category.show', ['category' => $category]);
    }

    public function edit(Category $category, StoreCategoryRequest $storeCategoryRequest)
    {
        $category->title = $storeCategoryRequest->title;
        $category->save();
        return redirect()->route('home.category.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }



}
