<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Models\Product;
use Doctrine\DBAL\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use LukePOLO\LaraCart\Facades\LaraCart;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index(Product $product)
    {
        $count = count(LaraCart::getItems());
        $products = $product::limit(5)->get();
        return view('welcome', ['products' => $products, 'count' => $count]);
    }

    public function addCart(Request $request)
    {

        LaraCart::add(
            $request->get('id'),
            $name = null,
            $qty = 1,
            $price = '0.00',
            $options = [],
            $taxable = true,
            $lineItem = false
        );

//        dd(LaraCart::getItems());

        return redirect()->route('home.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(StoreProductRequest $storeProductRequest)
    {
        dd($storeProductRequest->validated());
        try {
            Product::create($storeProductRequest->all());
        } catch (Exception $exception) {
            Log::error($exception);
//            return 'error';
        }

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
