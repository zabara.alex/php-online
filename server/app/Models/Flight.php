<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Flight extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'flights';
//    protected $primaryKey = 'flight_id';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'age',
    ];

//    protected $guarded = [];

//    protected $attributes = [
//        'name_test' => 'Alex'
//    ];

}
