<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'user_id',
        'order',
    ];

    public $timestamps = false;
    protected $table = 'test_orders';
    protected $connection = 'mysql';
    protected $hidden = ['password', 'token'];
    protected $casts = ['options' => 'array'];

}
